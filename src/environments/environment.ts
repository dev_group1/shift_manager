// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyAPn_7y3Yscaw-fBHy-MT_IcHh7bhTSun4',
        authDomain: 'shift-manager-92d3e.firebaseapp.com',
        databaseURL: 'https://shift-manager-92d3e.firebaseio.com',
        projectId: 'shift-manager-92d3e',
        storageBucket: 'shift-manager-92d3e.appspot.com',
        messagingSenderId: '327839307743'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
