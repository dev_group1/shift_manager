import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticateService} from '../services/authentication.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.scss'],
})
export class UserNavbarComponent implements OnInit {

  userEmail: string;
  @ViewChild('userSideMenu') userSideMenu;

  constructor(private authService: AuthenticateService,
              private navCtrl: NavController
  ) { }

  ngOnInit() {
    if (this.authService.userDetails()) {
      this.userEmail = this.authService.userDetails().email;
    } else {
      this.navCtrl.navigateBack('');
    }
  }

  openMenu() {
    this.userSideMenu.openFirst();
  }

}
