import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import {UserNavbarComponent} from '../user-navbar/user-navbar.component';
import {UserSideMenuComponent} from "../user-side-menu/user-side-menu.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [UserNavbarComponent, UserSideMenuComponent],
  exports: [ UserNavbarComponent]
})
export class UserNavbarModule {}
