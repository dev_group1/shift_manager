import { Component, OnInit } from '@angular/core';
import {MenuController, NavController} from "@ionic/angular";
import {AuthenticateService} from "../services/authentication.service";

@Component({
  selector: 'app-user-side-menu',
  templateUrl: './user-side-menu.component.html',
  styleUrls: ['./user-side-menu.component.scss'],
})
export class UserSideMenuComponent implements OnInit {

  constructor(private menu: MenuController,
              private authService: AuthenticateService,
              private navCtrl: NavController,) { }

  ngOnInit() {}

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  logOut() {
    this.authService.logoutUser()
        .then(res => {
          console.log(res);
          this.navCtrl.navigateBack('');
        })
        .catch(error => {
          console.log(error);
        });
  }
}
