import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyledLoginPage } from './styled-login.page';

describe('StyledLoginPage', () => {
  let component: StyledLoginPage;
  let fixture: ComponentFixture<StyledLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyledLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyledLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
