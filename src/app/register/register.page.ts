import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticateService } from '../services/authentication.service';
import { NavController } from '@ionic/angular';
import {FirebaseDbService} from '../services/firebase-db.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    validations_form: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';

    validation_messages = {
        'email': [
            { type: 'required', message: 'נדרש להזין אימייל' },
            { type: 'pattern', message: 'הזינו אימייל תקין' }
        ],
        'password': [
            { type: 'required', message: 'נדרש להזין סיסמא' },
            { type: 'minlength', message: 'אורך הסיסמא חייב להיות 5 לפחות' }
        ]
    };

    constructor(
        private navCtrl: NavController,
        private authService: AuthenticateService,
        private firebaseDbService: FirebaseDbService,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit(){
        this.validations_form = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.minLength(5),
                Validators.required
            ])),
        });
    }

    tryRegister(value){
        this.authService.registerUser(value)
            .then(res => {
                console.log(res);
                this.firebaseDbService.addUserToDb().then(res => {
                    this.errorMessage = "";
                    this.successMessage = "Your account has been created. Please log in.";
                });
            }, err => {
                console.log(err);
                this.errorMessage = err.message;
                this.successMessage = "";
            })
    }

    goLoginPage(){
        this.navCtrl.navigateBack('');
    }


}