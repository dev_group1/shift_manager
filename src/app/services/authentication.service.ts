import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from "@angular/fire/auth";

@Injectable()
export class AuthenticateService {

    constructor(private auth: AngularFireAuth) {}

    provider;

    registerUser(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then(
                    res => resolve(res),
                    err => reject(err));
        });
    }

    loginUser(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(value.email, value.password)
                .then(
                    res => resolve(res),
                    err => reject(err));
        });
    }

    logoutUser() {
        return new Promise((resolve, reject) => {
            if (firebase.auth().currentUser) {
                firebase.auth().signOut()
                    .then(() => {
                        console.log('LOG Out');
                        resolve();
                    }).catch((error) => {
                    reject();
                });
            }
        });
    }

    googleUserLogin() {
        this.provider = new firebase.auth.GoogleAuthProvider();

        this.provider.setCustomParameters({
            'login_hint': 'user@example.com'
        });


        return new Promise<any>((resolve, reject) => {
            this.auth.auth.signInWithPopup(this.provider)
                .then(
                    res => resolve(res),
                    err => reject(err));
        });
    }

    userDetails() {
        return firebase.auth().currentUser;
    }
}
