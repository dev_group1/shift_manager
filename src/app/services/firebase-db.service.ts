import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDbService {

  constructor(private firestore: AngularFirestore) { }

    addUserToDb() {
        return new Promise<any>((resolve, reject) => {
            const currentUser = firebase.auth().currentUser;
            this.firestore.collection('users').doc(currentUser.uid).set({
                displayName: 'shahar',
                imageUrl: 'Ggggfdg'
            })
                .then(
                    res => resolve(res),
                    err => reject(err)
                );
        });
    }

    read_Students() {
        return this.firestore.collection('Students').snapshotChanges();
    }

    update_Student(recordID,record){
        this.firestore.doc('Students/' + recordID).update(record);
    }

    delete_Student(record_id) {
        this.firestore.doc('Students/' + record_id).delete();
    }
}
